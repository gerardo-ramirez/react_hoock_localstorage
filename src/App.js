import React, { useState, useEffect } from 'react';
import './App.css';
import { TaskRow } from './components/TaskRow';
import { TaskBanner } from './components/TaskBanner';
import { TaskCreator } from './components/TaskCreator';
import { VisibilityControl } from './components/VisibilityControl'
function App() {
  const [userName, setUserName ]= useState('gerar');
  const [taskItem, setTaskIntem]= useState([
    {name:'task one', done: false },
    {name:'task two', done: false },
    {name:'task three', done: true},
    {name:'task four', done: false },
  ]);

  //usamos el useEffect para guardar en el localstorage:

  useEffect(()=>{
    //nos fijamos si existe
    let data = localStorage.getItem('tasks');
    //guardamos en taskItem la data
    if(data != null){
      setTaskIntem(JSON.parse(data));
    } else{
      setUserName("Ejemplos Local");
      setTaskIntem([
        {name:'task one localstorage', done: false },
        {name:'task two localstorage', done: false },
        {name:'task three localstorage', done: true},
        {name:'task four localstorage', done: false },
      ]);
      setShowCompleted(true);
    }
  },[]);

  //usamos un segundo useEffect:
  //este se ejecutará cada vez que el taskItem Cambie
  useEffect(()=>{
    localStorage.setItem('tasks', JSON.stringify(taskItem));

  },[taskItem]);
  //creamos un estado que  nos devuelva true o false en las tareas hechas para el componente Visibitity
  const [showCompleted, setShowCompleted]= useState(true);
  //funcion marcar tarea
  const toggleTask = (task)=> {

    setTaskIntem(taskItem.map(t => t.name === task.name? {...t, done: !t.done}: t ))
  }
  //mostrar tabla de tareas:
const TaskTableRow = (doneValue) =>{
//filtramos las tareas a mostrar para separar la hechas de las par hacer
 
return  taskItem.filter(t => t.done === doneValue).map(task =>(
    //como le paso la funcion por props no lleva parentesis aqui.
    <TaskRow task ={task} key={task.name} toggleTask={toggleTask}/>
  ))
 
};
//crear tarea:
const createNewTask = (taskName)=>{
  if(!taskItem.find(ti => ti.name === taskName)){
    //es un arreglo de objetos asi que se usa ... para el arreglo y agregamos un objeto mas
    setTaskIntem([...taskItem, {name: taskName , done: false}])
  }
}

  return (
    <div className="App">
      {// le paso todas las tareas y voy a filtrarlas
}
  <TaskBanner userName = {userName} cantidadTask = {taskItem}/>
  {// le pasamos la funcion para crear tareas a taskCreator
  }
  <TaskCreator callback ={createNewTask}/>
  <table className="table table-striped table-borded">
    <tr>
      <th>Description</th>
      <th>Done</th>
    </tr>
     <tbody>
    {TaskTableRow(false)}
  </tbody>
  </table>
  <div className="bg-secondary-text-white text-center p-2">
    <VisibilityControl description="completed task" 
    callback={check=> setShowCompleted(check)}
    isChecked={showCompleted} />
  </div>{
     showCompleted && (
       <table className="table table-striped table-bordered">
        <thead>
          <tr>
            <th>description</th>
            <th>Done</th>

          </tr>
        </thead>
        <tbody>
          {TaskTableRow(true)}
        </tbody>
       </table>
     )
    
    
  }
 
    </div>
  );
}

export default App;
