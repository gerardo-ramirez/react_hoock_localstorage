import React, {useState} from 'react';



export const TaskCreator = (props) =>{
    const [newTakName, setNewTaskName]= useState('');
    //creamos una funcion para actualizar tarea:
const updateTaskvalue = e => setNewTaskName(e.target.value);
//funcion para agregar: 
const createNewTask = ()=>{
    props.callback(newTakName);
    console.log(newTakName);
    setNewTaskName('');
} ;




return (
    <div className="my-1">
        <input type='text' className='form-control' value={newTakName}
        onChange={updateTaskvalue}
        />
        
    <button onClick={()=>createNewTask()} className={"btn btn-primary mt-1"}>Add</button>
    </div>

)
};
