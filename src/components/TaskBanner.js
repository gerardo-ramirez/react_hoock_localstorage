import React from 'react';

export const TaskBanner = (props)=>(
    <h4 className='bg-primary text-white text-center p-4'>
        Tareas de {props.userName}, quedan :
         {props.cantidadTask.filter( f => !f.done).length}
    </h4>
);