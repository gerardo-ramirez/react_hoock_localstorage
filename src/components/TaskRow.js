import React, { useState} from 'react';
export const TaskRow = ( props)=>( 
     
        <tr key= {props.task.name}>
          <td>{props.task.name}</td>
          <td>{//aquí si utilizo la funcion entonces uso parentesis y le paso los parametros.
          }
              <input type='checkbox' checked={props.task.done} onChange={()=>props.toggleTask(props.task)}></input>
          </td>
        </tr>
     
);